# todo-strapi

Simple todo app to test Strapi backend and Backbone/Marionette ES6 frontend

## Installation

Install global dependencies

```
~/www/$ npm i -g strapi webpack
```

Clone the project

```
~/www/$ git clone git@gitlab.com:dff/todo-strapi.git && cd todo-strapi
```

Install local dependencies

```
~/www/todo-strapi$ npm i
```

## Run project

To run the project in dev mode simply execute the following command:

```
~/www/todo-strapi$ webpack --watch & strapi start
```

This will run webpack builds with watch mode activated to re-build each time a file changes and
start the strapi server. Then simply connect to http://localhost:1337/ to see the result.
