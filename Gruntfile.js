'use strict';

module.exports = (grunt) => {
  const path = require('path');

  require('time-grunt')(grunt);

  // configurable paths
  var configPath = {
    app: 'app',
    tmp: '.tmp',
    dist: 'dist',
    css: 'styles',
    js: 'scripts',
    images: 'img'
  };

  var config = {
    config: configPath,
    options: {
      port: grunt.option('port') || 3000,
      hostname: grunt.option('host') || 'localhost',
      https: grunt.option('secure') || false,
      browser: grunt.option('browser') || 'google-chrome'
    }
  };

  require('load-grunt-config')(grunt, {
    configPath: path.join(process.cwd(), 'tasks'),
    init: true,
    config: config
  });
};
