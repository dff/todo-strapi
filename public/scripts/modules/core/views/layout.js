// External deps
import {LayoutView} from 'backbone.marionette';

// Views
import HeaderView from './header';
import TodosLayout from 'todos/views/layout';

// Templates
import LayoutTemplate from 'core/templates/layout';

export default LayoutView.extend({
  el: '#main-container',

  template: LayoutTemplate,

  regions: {
    header: '[data-region=header]',
    content: '[data-region=content]'
  },

  onRender() {
    this.header.show(new HeaderView());
    this.content.show(new TodosLayout());
  }
});
