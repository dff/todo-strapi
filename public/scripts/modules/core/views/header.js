// External deps
import {ItemView} from 'backbone.marionette';

// Templates
import HeaderTemplate from 'core/templates/header';

// Styles
import 'core/styles/header.scss';

export default ItemView.extend({
  template: HeaderTemplate
});
