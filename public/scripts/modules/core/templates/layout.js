import _ from 'lodash';

export default _.template(`
  <header data-region="header"></header>
  <main data-region="content"></main>
`);
