import _ from 'lodash';

export default _.template(`
  <span class="remaining"><%= remaining %> remaining</span>
  <span class="actions">
    <a href="#" class="clear-done">Clear done</a>
  </span>
`);
