import _ from 'lodash';

export default _.template(`
  <form class="todo-form">
    <button type="button" class="all-done">&gt;</button>
    <input name="todo-title" placeholder="What needs to be done?" value="" autocomplete="off">
    <button type="submit">Add</button>
  </form>
`);
