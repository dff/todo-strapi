import _ from 'lodash';

export default _.template(`
  <div data-region="add-form"></div>
  <div data-region="list"></div>
  <div data-region="status"></div>
`);
