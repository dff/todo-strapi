import _ from 'lodash';

export default _.template(`
  <span class="<%= className %>">
    <button class="set-todo-state">
      <%= icon %>
    </button>
    <span class="title">
      <%= title %>
    </span>
    <button class="delete">
      &#x2715;
    </button>
  </span>
`);
