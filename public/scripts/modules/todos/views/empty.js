// External deps
import {ItemView} from 'backbone.marionette';

// Templates
import EmptyTodoView from 'todos/templates/empty';

// Styles
import 'todos/styles/empty.scss';

export default ItemView.extend({
  template: EmptyTodoView,

  tagName: 'li',
  className: 'empty'
});
