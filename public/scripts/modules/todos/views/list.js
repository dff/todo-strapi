// External deps
import log from 'loglevel';
import {CollectionView} from 'backbone.marionette';

// Views
import EmptyView from './empty';
import TodoItemView from  './item';

export default CollectionView.extend({
  tagName: 'ul',
  childView: TodoItemView,
  emptyView: EmptyView
});
