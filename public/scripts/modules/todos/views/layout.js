// External deps
import {LayoutView} from 'backbone.marionette';

// Utils
import todosUtil from 'utils/todos';

// Views
import TodosList from './list';
import AddTodoForm from './form';
import Status from './status';

// Templates
import TodosLayout from 'todos/templates/layout';

export default LayoutView.extend({
  template: TodosLayout,

  regions: {
    list: '[data-region=list]',
    form: '[data-region=add-form]',
    status: '[data-region=status]'
  },

  onBeforeShow() {
    todosUtil.getList().then(todos => {
      this.list.show(
        new TodosList({
          collection: todos
        })
      );

      this.form.show(
        new AddTodoForm({todos})
      );

      this.status.show(
        new Status({todos})
      );
    });
  }
});
