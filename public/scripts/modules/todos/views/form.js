// External deps
import log from 'loglevel';
import {ItemView} from 'backbone.marionette';

// Utils
import todosUtil from 'utils/todos';

// Templates
import FormTemplate from 'todos/templates/form';

// Styles
import 'todos/styles/form.scss';

export default ItemView.extend({
  template: FormTemplate,

  ui: {
    title: '[name=todo-title]',
    allDone: '.all-done'
  },

  events: {
    'submit form': 'addTodo',
    'click @ui.allDone': 'setAllDone'
  },

  initialize() {
    this.todos = this.options.todos;
    this.todos.on('update', () => this.setAllDoneVisibility());
  },

  onRender() {
    this.setAllDoneVisibility();
  },

  addTodo(e) {
    e.preventDefault();

    todosUtil.create({
        title: this.ui.title.val(),
        done: false
      })
      .then(model => {
        log.debug('[ views/todos/form ] Stored new todo in database');

        this.todos.add(model);
        this.ui.title.val('');
      });
  },

  setAllDone(e) {
    e.preventDefault();
    todosUtil.setAllDone(this.todos);
  },

  setAllDoneVisibility() {
    if (this.todos.length < 1) {
      this.ui.allDone.addClass('no-todos');
    }
    else {
      this.ui.allDone.removeClass('no-todos');
    }
  }
});
