// External deps
import {Model} from 'backbone';
import {ItemView} from 'backbone.marionette';

// Utils
import todosUtil from 'utils/todos';

// Templates
import StatusTemplate from 'todos/templates/status';

// Styles
import 'todos/styles/status.scss';

export default ItemView.extend({
  template: StatusTemplate,
  className: 'status-container',

  ui: {
    clearDone: '.clear-done'
  },

  events: {
    'click @ui.clearDone': 'clearDone'
  },

  initialize() {
    this.todos = this.options.todos;
    this.todos.on('update', () => this.updateComponents());
    this.todos.on('change', () => this.updateComponents());

    this.model = new Model({
      remaining: todosUtil.getRemainingCount(this.todos)
    });
  },

  onRender() {
    this.setClearDoneVisibility();
  },

  updateComponents() {
    this.setTodoStatus();
    this.setClearDoneVisibility();
  },

  setTodoStatus() {
    this.model.set(
      'remaining',
      todosUtil.getRemainingCount(this.todos)
    );

    this.render();
  },

  clearDone(e) {
    e.preventDefault();

    let doneTodos = this.todos.filter(todo => todo.get('done'))

    todosUtil.destroyList(doneTodos);
  },

  setClearDoneVisibility() {
    if (todosUtil.getDoneCount(this.todos) > 0) {
      this.ui.clearDone.show();
    }
    else {
      this.ui.clearDone.hide();
    }
  }
});
