// External deps
import log from 'loglevel';
import {ItemView} from 'backbone.marionette';

// Utils
import todosUtil from 'utils/todos';

// Templates
import TodoItemTemplate from 'todos/templates/item';

// Styles
import 'todos/styles/item.scss';

// Todo UI helper states
const ui = {
  done: {
    className: 'done',
    icon: '&#x2611;'
  },
  todo: {
    className: '',
    icon: '&#x2610;'
  }
};

export default ItemView.extend({
  template: TodoItemTemplate,
  tagName: 'li',

  events: {
    'click .set-todo-state': 'setDone',
    'click .delete': 'deleteItem'
  },

  modelEvents: {
    change: 'render'
  },

  templateHelpers() {
    return this.model.get('done') ? ui.done : ui.todo;
  },

  setDone(e) {
    e.preventDefault();
    this.model.set('done', !this.model.get('done'));
    todosUtil.update(this.model);
  },

  deleteItem(e) {
    e.preventDefault();
    todosUtil.destroy(this.model);
  }
});
