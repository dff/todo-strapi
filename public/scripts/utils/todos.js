// External deps
import log from 'loglevel';
import {Model, Collection} from 'backbone';

export default {
  getList() {
    return new Promise((resolve, reject) => {
      let todos = new Collection();

      todos.url = '/todo';

      todos.fetch({
        success: collection => {
          log.debug('[ utils/todos ] Fetched todos');
          resolve(collection);
        },
        error: () => {
          reject(new Error('[ utils/todos ] Could not fetch todos'));
        }
      });
    });
  },

  create(data) {
    return new Promise((resolve, reject) => {
      let todo = new Model();
      todo.urlRoot = '/todo';

      todo.save(data, {
        success: model => {
          log.debug('[ utils/todos ] Created new todo');
          resolve(model);
        },
        error: () => {
          reject(new Error('[ utils/todos ] Could not save new todo'));
        }
      });
    });
  },

  update(model) {
    return new Promise((resolve, reject) => {
      model.save(null, {
        success: model => {
          log.debug('[ utils/todos ] Updated todo');
          resolve(model);
        },
        error: () => {
          reject(new Error('[ utils/todos ] Could not update todo data'));
        }
      });
    });
  },

  destroy(model) {
    return new Promise((resolve, reject) => {
      model.destroy({
        success: model => {
          log.debug('[ utils/todos ] Destroyed todo');
          resolve(model);
        },
        error: () => {
          reject(new Error('[ utils/todos ] Could not destroy todo'));
        }
      });
    })
  },

  destroyList(todos) {
    return Promise.all(
      todos.map(todo => this.destroy(todo))
    );
  },

  setAllDone(todos) {
    let allDone = this.getRemainingCount(todos) === 0;

    return Promise.all(
      todos.map(todo => {
        todo.set('done', !allDone);
        return this.update(todo);
      })
    );
  },

  getRemainingCount(todos) {
    return todos.filter(todo => !todo.get('done')).length;
  },

  getDoneCount(todos) {
    return todos.filter(todo => todo.get('done')).length;
  }
};
