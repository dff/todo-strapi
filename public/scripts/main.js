// External deps
import {Model} from 'backbone';
import {LayoutView} from 'backbone.marionette';

// Views
import MainLayout from 'core/views/layout';

// Styles
import 'core/styles/layout.scss';

// Initialize main view ...
const layout = new MainLayout();

// ... and render it!
layout.render();
